# TRAEFIK

Traefik est un applicatif pouvant servir de reverse proxy mappant les ports 80 et 443 de l'hôte et créant les routes vers les conteneurs avec leur certificats SSL.

## CONFIGURATION

- Modifier l'email pour les certificats HTTPS:
```bash
nano .env
```
- Pour ajouter un service à Traefik:
```yml
# MODIFIER DNS DANS CHAQUE LABELS APRES ROUTERS
    labels:
      # HTTP
      - "traefik.enable=true"
      - "traefik.http.routers.whoami.entrypoints=web"
      # DNS
      - "traefik.http.routers.whoami.rule=Host(`whoami.localhost`)"

      # HTTPS
      - traefik.http.routers.whoami-ssl.tls.certresolver=le
      - traefik.http.routers.whoami-ssl.entryPoints=websecure
      - traefik.http.routers.whoami-ssl.tls=true
      # DNS
      - traefik.http.routers.whoami-ssl.rule=Host(`whoami.localhost`)
      # SERVICE
      - traefik.http.routers.whoami-ssl.service=whoami
```

> Note: Les labels sont à ajouter dans le compose du service à rattacher à Traefik


## UTILISATION

- Lancer la stack:
```bash
docker-compose up -d
```

## DOCUMENTATION

> https://doc.traefik.io/traefik/
